<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## 15/01/2021
- Installation: https://laravel.com/docs/8.x/installation

  --------------------------------------------------------------------------------------
  | C1: composer create-project --prefer-dist laravel/laravel:'version' 'name_project' |

  --------------------------------------------------------------------------------------
  --------------------------------------------------
  | C2: composer global require laravel/installer  |
  | => laravel new name_project                    |
  --------------------------------------------------

- Auth:

  -------------------------------
  | composer require laravel/ui |
  -------------------------------

  -----------------------------------
  | php artisan ui bootstrap --auth |
  -----------------------------------

- config:

  + Mở app > Providers > AppServiceProvider > config:

  ------------------------------------------
  | use Illuminate\Support\Facades\Schema; |
  | public function boot()                 |
  | {                                      |
  |   Schema::defaultStringLength(191);    |
  | }                                      |
  ------------------------------------------

- Laravel debugbar: https://laravel-news.com/laravel-debugbar

  ----------------------------------------------
  | composer require barryvdh/laravel-debugbar |
  ----------------------------------------------

## 16/01/2021
  - Config > database > Đổi thông số ở 'mysql'
  - Tạo Middleware

    ----------------------------------------------
    | php artisan make:middleware tên_middleware |
    ----------------------------------------------

  * Middleware Laravel: https://laravel.com/docs/8.x/middleware

  - Kernel > Khởi tạo Middleware
  - Tạo trang login admin: (BE)

    +> Mục tiêu: Thực hiện chức năng đăng nhập, đăng xuất (normal)

    +> Phương pháp: Dùng Auth > Config lại Models, Auth

## 17/01/2021
  - Thêm trang chủ khách hàng (FE)
  - Thêm trang quên mật khẩu (BE)
  - Thêm chức năng quản lý danh mục sản phẩm (BE)

    +> Thêm & sửa & xoá
    +> Kích hoạt trạng thái

  - Tạo factory & seeder bảng danh mục sản phẩm (Fetch-data Demo)
    +> Generating Factories  
    -------------------------------------------------------------
    | php artisan make:factory CategoryFactory --model=Category |
    -------------------------------------------------------------
    +> Writing Seeders
    ------------------------------------------
    | php artisan make:seeder CategorySeeder |
    ------------------------------------------
    +> Running Seeders
      ~> Seeders tất cả
      -----------------------
      | php artisan db:seed |
      -----------------------
      ~> Seeders những bảng chỉ định
      ----------------------------------------------
      | php artisan db:seed --class=CategorySeeder |
      ----------------------------------------------

## 19/01/2021
  - Thêm chức năng quản lý hiệu sản phẩm

    +> Thêm & sửa & xoá 
    +> Kích hoạt trạng thái 
    
  - Thêm lối tắt thư mục sidebar

## 24/01/2021
  - Thêm chức năng quản lý sản phẩm

    +> Thêm & sửa & xoá
    +> Kích hoạt trạng thái

## 25/01/2021
  - Hiển thị sản phẩm lên trang khách hàng

## 27/01/2021
  - Thêm cp phần tin tức + tin tức chi tiết (FE)

## 28/01/2021
  - Thêm lọc sản phẩm theo danh mục và thương hiệu (FE)
  - Thêm hiển thị chi tiết sản phẩm (FE)
  
## 29/01/2021
  - Thêm chức năng giỏ hàng (FE)
  
  - Xử lý giỏ hàng
  +> Import thư viện 
  ---------------------------------------------
  | composer require bumbummen99/shoppingcart |
  ---------------------------------------------

  --------------------------------------------------------------------------------------------------------------
  | php artisan vendor:publish --provider="Gloudemans\Shoppingcart\ShoppingcartServiceProvider" --tag="config" |
  --------------------------------------------------------------------------------------------------------------

  - Hoàn thành chức năng thêm, cập nhật số lượng, hiển thị view giỏ hàng,... 

## 06/02/2021
  - Thêm chức năng thanh toán
  +> Chức năng đăng nhập, đăng ký cho khách hàng

## 07/02/2021
 - Hoàn thành chức năng đăng ký và đăng nhập

## 08/02/2021
 - Thêm chức năng ship

## 10/02/2021
 - Thanh toán(payment)
 - Đặt hàng

 * Note: Rollback dữ liệu migration
 --------------------------------
 | php artisan migrate:rollback |
 --------------------------------

## 17/02/2021
 - Tích hợp CK-editor
 - LINK: https://ckeditor.com/

## 24/02/2021
 - Hoàn thành tính mã giảm giá cho sản phẩm

## 25/02/2021
 - Tích hợp ship hàng qua tỉnh + quận + thành phố
 
## 27/02/2021
 - Thay thế shopping cart (vendor) thành cart ajax
 - Thêm giỏ hàng, tự động cập nhật giỏ hàng
 - Xoá 1 hoặc nhiều sản phẩm trong giỏ hàng, 
 
## 28/03/2021
 - Hoàn thành xong sơ sơ phần giỏ hàng
 
## 01/03/2021
 - Sửa một số cột trong 1 số bảng: shipping, order, order_details
 - Xoá bảng payment
 - Sửa lỗi phần đặt hàng
 - Thực hiện xác nhận đơn hàng
 
## 02/03/2021
 - Hiển thị đơn hàng (coupon, shipping)
 - In ra đơn hàng (PDF)
    + Link: https://github.com/barryvdh/laravel-dompdf
    + Import các thư viện 
    --------------------------------------------
    | composer require barryvdh/laravel-dompdf |
    --------------------------------------------
    
    ------------------------------------------
    | Barryvdh\DomPDF\ServiceProvider::class |
    -----------------------------------------
        
    -------------------------------------------
    | 'PDF' => Barryvdh\DomPDF\Facade::class, |
    -------------------------------------------
    
    --------------------------------------------------------------------------
    | php artisan vendor:publish --provider="Barryvdh\DomPDF\ServiceProvider"|
    --------------------------------------------------------------------------

## 03/03/2021
 - Hoàn thành in đơn hàng 

## 27/03/2021
 - Thêm phần quản lý giao diện > slider
    +> Thêm, sửa, xóa, hiển thị trên UI
 - Import & Excel một số bảng: 
    +> Link: https://docs.laravel-excel.com/3.1/getting-started/
    +> Import thư viện
    --------------------------------------
    | composer require maatwebsite/excel |
    --------------------------------------
    
    --------------------------------------------------
    | Maatwebsite\Excel\ExcelServiceProvider::class, |
    --------------------------------------------------
        
    ------------------------------------------------------
    | 'Excel' => Maatwebsite\Excel\Facades\Excel::class, |
    ------------------------------------------------------

    -----------------------------------------------------------------------------------------------
    | php artisan vendor:publish --provider="Maatwebsite\Excel\ExcelServiceProvider" --tag=config |
    -----------------------------------------------------------------------------------------------
    
## 28/03/2021
 - Quản lý số lượng tồn đọng (BE)
 - Tích hợp f-chat (FE)
 - Styling trang 404, 500 (FE)
 - Quản lý số lượng đặt hàng (FE)

## Note: Fix lại từ version 8 -> 7 [2/4/2021]

## 04/04/2021
 - Chức năng đăng ký (BE)
 - Xây dựng chức năng phân quyền
    ---------------------------------------------
    | php artisan make: seeder RolesTableSeeder |
    ---------------------------------------------

    ---------------------------------------------
    | php artisan make: seeder UsersTableSeeder |
    ---------------------------------------------
    
    -----------------------
    | php artisan db:seed |
    -----------------------

 - Cấp quyền cho từng đối tượng

## 05/04/2021
 - Fake dữ liệu cho bảng admin
    --------------------------------------------------------------
    | php artisan make:factory AdminFactory --model=Models/Admin |
    --------------------------------------------------------------
    
 - Web faker tham khảo: https://github.com/fzaninotto/Fakers
 - Tinker: 
    ----------------------
    | php artisan tinker |
    ----------------------

 - Tạo 1 provider xử lý hạn chế quyền cho user
    --------------------------------------------------
    | php artisan make:provider BladeServiceProvider |
    --------------------------------------------------

 - Web tham khảo: https://www.amitmerchant.com/create-your-own-blade-directive-laravel/

## 20/04/2021
 - Chức năng thêm, sửa, xóa bài viết
 - Hiển thị bài viết lên trang web 
 - Tìm kiếm bài viết

## 25/04/2021
 - Thêm, sửa, xóa thư viện ảnh
 - Hiển thị thư viện ảnh lên trang web

## 27/04/2021
 - Lọc dữ liệu tìm kiếm (B-Category)
 - Thêm thẻ tag cho sản phẩm (B-Product)
 
## 28/04/2021
 - Tìm kiếm sản phẩm tự động (FE) (AutoComplete)
 - Chức năng xem nhanh sản phẩm (Quickview)

## 01/05/2021
 - Load đánh giá sản phẩm 
 - Một số chức năng đánh giá (Làm sau)

## 03/05/2021
 - Chức năng thêm, sửa, xóa thông tin liên hệ
 - Hiển thị thông tin liên hệ lên website
 - Update ckeditor (Upload và lấy ảnh cục bộ)

 - Thêm sản phẩm yêu thích (local Storage)
    + localStorage.setItem(key, value)
    + localStorage.getItem(key)
    + localStorage.removeItem(key)
    + localStorage.clearAll()
 - Hiển thị sản phẩm yêu thích lên website
 
## 04/05/2021
 - Lọc sản phẩm theo tên (A-Z, Z-A)
 - Lọc theo giá (tăng dần, giảm dần)
 - Bộ lọc (slider + range)
 - Thư viện simple money format.js

## 06/05/2021
 - Chức năng gửi mail khuyến mãi cho khách hàng (vip)
  + Fix database (customer)
  + Fix model (customer)
  + Cấu hình lại mail trong .env
  + Cấu hình lại file config/mail.php

 - Ngày hết hạn giảm giá
  + Fix database (coupon)
  + Fix model (coupon)  

## 11/05/2021

 - Kiểm tra ngày mã giảm giá hết hạn
 - Giảm số lượng mã giảm giá khi đã đặt hàng
 - Mỗi mã giảm giá chỉ nhập 1 lần cho 1 người dùng 
 - Gửi từng mã khuyến mãi cho khách hàng qua email

## 12/05/2021
 
 - Quên mật khẩu khách hàng
 <Bổ sung: Check tối đa reset 3 lần trong 1 ngày>

## 13/05/2021

 - Đăng nhập bằng google: (khách hàng)
  + Thư viện: composer require laravel/socialite
  + B1: Truy cập trang https://console.cloud.google.com/
  + B2: Click Enable API and services > Social > Google+ API > Enable
  + B3: Cấu hình cho .env
  + B4: Cấu hình file config/services.php

## 14/05/2021

 - Đăng nhập bằng facebook: (khách hàng)
 - Tương tự các bước làm với google
 - Một số khác:
  + https://developers.facebook.com/apps/
 - Fix 1 số lỗi
 - Xác nhận đặt hàng

## 16/05/2021

 - Tích hợp thanh toán online Paypal
 - Paypal: https://developer.paypal.com/home
 - kelvinanh691@gmail.com | MK: 
 - guccishopx2@business.example.com | MK: 

## 19/05/2021
 
 - Thêm OwlCarousel chạy tin tức tự động
 - Link: https://owlcarousel2.github.io/OwlCarousel2/
 - Thay đổi ngôn ngữ website:
    ----------------------------------------------------
    | composer require astrotomic/laravel-translatable |
    ----------------------------------------------------
 - Quản lý hình ảnh bằng Laravel File Manager:
    -------------------------------------------------
    | composer require unisharp/laravel-filemanager |
    -------------------------------------------------

    https://bit.ly/3hBcy0c

    - Chỉnh đường dẫn .env

## 20/05/2021 + 21/05/2021 + 22/05/2021
 
 - Thống kê doanh số đơn hàng
 - Links: http://morrisjs.github.io/morris.js/
 - Links: https://www.amcharts.com/demos/
 - Ý tưởng:
    + Thêm cột product_views và blog_views 
    + Mỗi lần click tăng lên số lần lên 1
- Cập nhật thống kê dựa vào bảng order

## 25/05/2021
 
 - Checklist ajax cho trang sản phẩm

## 30/05/2021

 - Thay đổi thông tin khách hàng

 - REGEX:
  - E-Mail            : ^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$
  - Phone             : ^[0-9\-\+]{9,15}$
  - Mật khẩu          : ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,16}$
  - Kiểm tra khớp MK  :  #mk

## 13/06/2021

 - Thêm vào phần quản trị admin
    + Danh sách khách hàng 
    + Thêm / Sửa / Xóa nhà cung cấp

## 14/06/2021

 - Thêm đánh giá sao
 - Chỉnh sửa cơ sở dữ liệu
 - Bình luận và đánh giá
