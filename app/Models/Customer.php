<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'customer_name',
        'customer_image',
        'customer_phone',
        'customer_address',
        'customer_email',
        'customer_password',
        'customer_vip',
        'customer_social',
        'customer_status',
    ];

    protected $primaryKey = 'customer_id';

    public $timestamps = false;

    public function array_order()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function rating(){
        return $this->hasMany('App\Models\Rating');
    }

    public function wishlist(){
        return $this->hasMany('App\Models\Wishlist');
    }
}
