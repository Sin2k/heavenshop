<?php

namespace App\Http\Controllers;

class AboutController extends Controller
{
    /**
     * GET: Hiển thị thông tin
     *
     * @return void
    */

    public function info()
    {
        $meta_title = "Cửa hàng bán quần áo thời trang Heaven | Thông tin";

        return view('pages.about')->with('meta_title', $meta_title);
    }
}
