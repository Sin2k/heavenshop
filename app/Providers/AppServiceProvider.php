<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Contact;
use App\Models\Wishlist;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view) {

            $app_contact = Contact::where('contact_status', 1)->get();

            $customer_id = Session::get('customer_id');

            $app_wishlist = Wishlist::with('product')->where('customer_id', $customer_id)->get();

            $view->with('app_contact', $app_contact)->with('app_wishlist', $app_wishlist);
        });
    }
}
