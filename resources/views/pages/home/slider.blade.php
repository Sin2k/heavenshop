<section class="section-slide hide-on-mobile-tablet p-b-50">
    <div class="wrap-slick1 rs1-slick1">
        <div class="slick1">
            @foreach ($sliders as $slider)
                <div class="item-slick1" style="background-image: url({{ asset('public/uploads/slider/'.$slider->slider_image) }})">
                    <div class="container h-full">
                        <div class="h-full flex-col-l-m p-t-100 p-b-30">
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
                                <span class="ltext-101 cl2 respon2">
                                    {{ $slider->slider_name }}
                                </span>
                            </div>

                            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                                    {!! $slider->slider_description !!}
                                </h2>
                            </div>

                            <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
                                <a href="{{ route('product') }}"
                                    class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                                    Mua sắm ngay
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
