<section class="sec-blog bg0 p-t-50 p-b-90">
    <div class="container">
        <div class="p-b-66">
            <h3 class="ltext-105 cl5 txt-center respon1">
                Tin tức
            </h3>
        </div>

        <div class="row">
            <div class="owl-carousel">
                @foreach ($posts as $post)
                    <div class="col l-4 p-b-40">
                        <div class="blog-item" style="width: 410px;">
                            <div class="hov-img0">
                                <a href="">
                                    <img src="{{ asset('public/uploads/blog/'.$post->post_image) }}" alt="IMG-BLOG">
                                </a>
                            </div>

                            <div class="p-t-15">
                                <div class="stext-107 flex-w p-b-14">
                                    <span class="m-r-3">
                                        <span class="cl4">
                                            By
                                        </span>

                                        <span class="cl5">
                                            {{ $post->post_author }}
                                        </span>
                                    </span>

                                    <span>
                                        <span class="cl4">
                                            on
                                        </span>

                                        <span class="cl5">
                                            {{ date('M', strtotime($post->post_date)) }}  {{ date('d', strtotime($post->post_date)) }}, {{ date('Y', strtotime($post->post_date)) }}
                                        </span>
                                    </span>
                                </div>

                                <h4 class="p-b-12 blog-title">
                                    <a href="" class="mtext-101 cl2 hov-cl1 trans-04">
                                        {{ $post->post_title }}
                                    </a>
                                </h4>

                                <p class="stext-108 cl6 blog-desc">
                                    {!! $post->post_description !!}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>
