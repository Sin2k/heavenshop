<!-- Product -->
<section class="sec-product bg0 p-b-50">
    <div class="container">
        <div class="p-b-32">
            <h3 class="ltext-105 cl5 txt-center respon1">
                Tổng quan về cửa hàng
            </h3>
        </div>

        <!-- Tab01 -->
        <div class="tab01">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item p-b-10">
                    <a class="nav-link active" data-toggle="tab" href="#new-product" role="tab">Sản phẩm mới</a>
                </li>

                <li class="nav-item p-b-10">
                    <a class="nav-link" data-toggle="tab" href="#featured-product" role="tab">Sản phẩm nổi bật</a>
                </li>

                <li class="nav-item p-b-10">
                    <a class="nav-link" data-toggle="tab" href="#selling-product" role="tab">Sản phẩm bán chạy</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content p-t-50">
                <div class="tab-pane fade show active" id="new-product" role="tabpanel">
                    <!-- Slide2 -->
                    <div class="wrap-slick2">
                        <div class="slick2">
                            @foreach ($new_product as $product)
                                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                    <!-- Block2 -->
                                    <div class="block2">
                                        <form>
                                            @csrf
                                            <input type="hidden" class="customer_id_{{ $product->product_id }}" value="{{ Session::get('customer_id') }}">
                                            <input type="hidden" value="{{ $product->product_quantity }}"
                                                class="product_quantity_{{ $product->product_id }}">
                                            <input type="hidden" value="{{ $product->product_name }}"
                                                class="wishlist_product_name_{{ $product->product_id }}">
                                            <input type="hidden" value="{{ number_format($product->product_price, 0,',','.') . ' ' . '₫' }}"
                                                class="wishlist_product_price_{{ $product->product_id }}">


                                            <a href="{{ route('product_detail', ['product_slug' => $product->product_slug]) }}">
                                                <div class="block2-pic hov-img0">
                                                    <img style="height: auto;"
                                                        class="wishlist_product_image_{{ $product->product_id }}"
                                                        src="{{ url('public/uploads/product/' . $product->product_image) }}"
                                                        alt="{{ $product->product_name }}">

                                                    <a href="#"
                                                        class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1 quick-view"
                                                        onclick="quickView({{ $product->product_id }})" data-toggle="modal"
                                                        data-target="#xemnhanh">
                                                        Xem nhanh
                                                    </a>

                                                    <button type="button" class="icon-cart add-to-cart" name="add-to-cart"
                                                        onclick="simpleAddCart({{ $product->product_id }})">
                                                        <i class="fa fa-cart-plus cc_pointer cart-plus-icon"></i>
                                                    </button>

                                                    @if (Session::get('customer_id'))

                                                        <button type="button" class="icon-wishlist add-to-wishlist"
                                                            name="add-to-wishlist"
                                                            onclick="addWishList({{ $product->product_id }})">
                                                            <i class="fa fa-heart"></i>
                                                        </button>

                                                    @else

                                                        <button type="button" class="icon-wishlist add-to-wishlist"
                                                            name="add-to-wishlist" onclick="checkWishList()">
                                                            <i class="fa fa-heart"></i>
                                                        </button>

                                                    @endif
                                                </div>
                                            </a>
                                        </form>

                                        <div class="block2-txt flex-w flex-t p-t-14">
                                            <div class="block2-txt-child1 flex-col-l ">
                                                <a href="{{ route('product_detail', ['product_slug' => $product->product_slug]) }}"
                                                    class="uppercase stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                    {{ $product->product_name }}
                                                </a>

                                                <span class="stext-105 cl3" style="font-family: system-ui;">
                                                    {{ number_format($product->product_price, 0, ',', '.') . ' ' . '₫' }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="featured-product" role="tabpanel">
                    <!-- Slide2 -->
                    <div class="wrap-slick2">
                        <div class="slick2">
                            @foreach ($featured_product as $product)
                                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                    <!-- Block2 -->
                                    <div class="block2">
                                        <form>
                                            @csrf
                                            <input type="hidden" class="customer_id_{{ $product->product_id }}" value="{{ Session::get('customer_id') }}">
                                            <input type="hidden" value="{{ $product->product_quantity }}"
                                                class="product_quantity_{{ $product->product_id }}">
                                            <input type="hidden" value="{{ $product->product_name }}"
                                                class="wishlist_product_name_{{ $product->product_id }}">
                                            <input type="hidden" value="{{ number_format($product->product_price, 0,',','.') . ' ' . '₫' }}"
                                                class="wishlist_product_price_{{ $product->product_id }}">


                                            <a href="{{ route('product_detail', ['product_slug' => $product->product_slug]) }}">
                                                <div class="block2-pic hov-img0">
                                                    <img style="height: auto;"
                                                        class="wishlist_product_image_{{ $product->product_id }}"
                                                        src="{{ url('public/uploads/product/' . $product->product_image) }}"
                                                        alt="{{ $product->product_name }}">

                                                    <a href="#"
                                                        class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1 quick-view"
                                                        onclick="quickView({{ $product->product_id }})" data-toggle="modal"
                                                        data-target="#xemnhanh">
                                                        Xem nhanh
                                                    </a>

                                                    <button type="button" class="icon-cart add-to-cart" name="add-to-cart"
                                                        onclick="simpleAddCart({{ $product->product_id }})">
                                                        <i class="fa fa-cart-plus cc_pointer cart-plus-icon"></i>
                                                    </button>

                                                    @if (Session::get('customer_id'))

                                                        <button type="button" class="icon-wishlist add-to-wishlist"
                                                            name="add-to-wishlist"
                                                            onclick="addWishList({{ $product->product_id }})">
                                                            <i class="fa fa-heart"></i>
                                                        </button>

                                                    @else

                                                        <button type="button" class="icon-wishlist add-to-wishlist"
                                                            name="add-to-wishlist" onclick="checkWishList()">
                                                            <i class="fa fa-heart"></i>
                                                        </button>

                                                    @endif
                                                </div>
                                            </a>
                                        </form>

                                        <div class="block2-txt flex-w flex-t p-t-14">
                                            <div class="block2-txt-child1 flex-col-l ">
                                                <a href="{{ route('product_detail', ['product_slug' => $product->product_slug]) }}"
                                                    class="uppercase stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                    {{ $product->product_name }}
                                                </a>

                                                <span class="stext-105 cl3" style="font-family: system-ui;">
                                                    {{ number_format($product->product_price, 0, ',', '.') . ' ' . '₫' }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="selling-product" role="tabpanel">
                    <!-- Slide2 -->
                    <div class="wrap-slick2">
                        <div class="slick2">
                            @foreach ($selling_product as $product)
                                <div class="item-slick2 p-l-15 p-r-15 p-t-15 p-b-15">
                                    <!-- Block2 -->
                                    <div class="block2">
                                        <form>
                                            @csrf
                                            <input type="hidden" class="customer_id_{{ $product->product_id }}" value="{{ Session::get('customer_id') }}">
                                            <input type="hidden" value="{{ $product->product_quantity }}"
                                                class="product_quantity_{{ $product->product_id }}">
                                            <input type="hidden" value="{{ $product->product_name }}"
                                                class="wishlist_product_name_{{ $product->product_id }}">
                                            <input type="hidden" value="{{ number_format($product->product_price, 0,',','.') . ' ' . '₫' }}"
                                                class="wishlist_product_price_{{ $product->product_id }}">

                                            <a href="{{ route('product_detail', ['product_slug' => $product->product_slug]) }}">
                                                <div class="block2-pic hov-img0">
                                                    <img style="height: auto;"
                                                        class="wishlist_product_image_{{ $product->product_id }}"
                                                        src="{{ url('public/uploads/product/' . $product->product_image) }}"
                                                        alt="{{ $product->product_name }}">

                                                    <a href="#"
                                                        class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1 quick-view"
                                                        onclick="quickView({{ $product->product_id }})" data-toggle="modal"
                                                        data-target="#xemnhanh">
                                                        Xem nhanh
                                                    </a>

                                                    <button type="button" class="icon-cart add-to-cart" name="add-to-cart"
                                                        onclick="simpleAddCart({{ $product->product_id }})">
                                                        <i class="fa fa-cart-plus cc_pointer cart-plus-icon"></i>
                                                    </button>

                                                    @if (Session::get('customer_id'))

                                                        <button type="button" class="icon-wishlist add-to-wishlist"
                                                            name="add-to-wishlist"
                                                            onclick="addWishList({{ $product->product_id }})">
                                                            <i class="fa fa-heart"></i>
                                                        </button>

                                                    @else

                                                        <button type="button" class="icon-wishlist add-to-wishlist"
                                                            name="add-to-wishlist" onclick="checkWishList()">
                                                            <i class="fa fa-heart"></i>
                                                        </button>

                                                    @endif
                                                </div>
                                            </a>
                                        </form>

                                        <div class="block2-txt flex-w flex-t p-t-14">
                                            <div class="block2-txt-child1 flex-col-l ">
                                                <a href="{{ route('product_detail', ['product_slug' => $product->product_slug]) }}"
                                                    class="uppercase stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                                    {{ $product->product_name }}
                                                </a>

                                                <span class="stext-105 cl3" style="font-family: system-ui;">
                                                    {{ number_format($product->product_price, 0, ',', '.') . ' ' . '₫' }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
