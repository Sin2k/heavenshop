<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_posts', function (Blueprint $table) {
            $table->increments('category_post_id')->comment('Mã danh mục bài viết');
            $table->string('category_post_name')->comment('Tên');
            $table->string('category_post_slug', 50)->unique()->comment('Slug');
            $table->string('category_post_description')->comment('Mô tả');
            $table->integer('category_post_status')->default('1')->comment('Trạng thái');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_posts');
    }
}
