<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social', function (Blueprint $table) {
            $table->increments('user_id')->comment('Mã người dùng');
            $table->string('provider_user_id', 100)->comment('Mã cung cấp');
            $table->string('provider_user_email', 100)->unique();
            $table->string('provider', 100)->comment('Tên');
            $table->unsignedInteger('user');
            $table->foreign('user')->references('customer_id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social');
    }
}
