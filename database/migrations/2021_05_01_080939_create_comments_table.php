<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('comment_id')->comment('Mã bình luận');
            $table->text('comment_content')->comment('Nội dung bình luận');
            $table->integer('comment_like')->default('0')->comment('Số lượt thích');
            $table->integer('comment_dislike')->default('0')->comment('Số lượt không thích');
            $table->integer('comment_status')->default('1')->comment('Trạng thái');
            $table->integer('parent_id')->unsigned()->nullable()->comment('Mã bình luận cha');
            $table->integer('admin_id')->unsigned()->nullable()->comment('Mã quản trị viên');
            $table->unsignedInteger('product_id')->comment('Mã sản phẩm');
            $table->unsignedInteger('customer_id')->comment('Mã khách hàng');
            $table->foreign('product_id')->references('product_id')->on('products')->onDelete('cascade');
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');
            $table->timestamp('comment_date')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('Ngày tạo');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
