<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeeshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeship', function (Blueprint $table) {
            $table->increments('fee_id')->comment('Mã phí ship');
            $table->unsignedInteger('city_id')->comment('Mã thành phố');
            $table->unsignedInteger('district_id')->comment('Mã quận/huyện');
            $table->unsignedInteger('commune_id')->comment('Mã phường/xã');
            $table->string('fee_feeship', 50)->comment('Giá ship');
            $table->foreign('city_id')->references('city_id')->on('cities')->onDelete('cascade');
            $table->foreign('district_id')->references('district_id')->on('districts')->onDelete('cascade');
            $table->foreign('commune_id')->references('commune_id')->on('communes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeship');
    }
}
