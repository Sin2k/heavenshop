<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
          $table->increments('id')->comment('Mã quản trị viên');
          $table->string('full_name', 50)->comment('Họ và tên');
          $table->string('user_name', 30)->unique()->comment('Tên tài khoản');
          $table->string('email', 100)->unique();
          $table->string('password', 100)->comment('Mật khẩu');
          $table->string('avatar', 100)->nullable()->comment('Hình đại diện');
          $table->char('phone', 20)->unique()->comment('Số điện thoại');
          $table->string('address', 100)->comment('Địa chỉ');
          $table->boolean('gender')->comment('Giới tính');
          $table->integer('status')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
