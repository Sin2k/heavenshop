<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatisticalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistical', function (Blueprint $table) {
            $table->increments('statistical_id')->comment('Mã thống kê');
            $table->string('order_date', 50)->comment('Ngày tạo');
            $table->string('funds', 50)->comment('Tiền vốn');
            $table->string('sales', 50)->comment('Doanh thu');
            $table->string('profit', 50)->comment('Lợi nhuận');
            $table->integer('quantity')->comment('Số lượng bán');
            $table->integer('total_order')->comment('Số hóa đơn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistical');
    }
}
