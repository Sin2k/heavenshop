<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id')->comment('Mã vai trò');
            $table->string('name', 50)->unique()->comment('Định danh');
            $table->string('display_name', 50)->comment('Tên vai trò');
        });

        Schema::create('role_admin', function (Blueprint $table) {
            $table->unsignedInteger('role_id')->comment('Mã vai trò');
            $table->unsignedInteger('admin_id')->comment('Mã người quản trị');
            $table->primary(['role_id', 'admin_id']);
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
        Schema::dropIfExists('role_admin');
    }
}
