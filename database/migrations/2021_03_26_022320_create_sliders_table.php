<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('slider_id')->comment('Mã slide');
            $table->string('slider_name', 100)->comment('Tên');
            $table->string('slider_image', 100)->nullable()->comment('Hình ảnh');
            $table->integer('slider_type')->comment('Loại');
            $table->integer('slider_status')->default('1')->comment('Trạng thái');
            $table->text('slider_description')->comment('Mô tả');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
