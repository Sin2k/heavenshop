<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping', function (Blueprint $table) {
            $table->increments('shipping_id')->comment('Mã người nhận');
            $table->string('shipping_name', 50)->comment('Tên');
            $table->char('shipping_phone', 20)->comment('Số điện thoại');
            $table->string('shipping_address', 100)->comment('Địa chỉ');
            $table->string('shipping_email', 100);
            $table->text('shipping_notes')->nullable()->comment('Ghi chú');
            $table->integer('shipping_method')->comment('Hình thức thanh toán');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping');
    }
}
