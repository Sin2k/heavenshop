<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('supplier_id')->comment('Mã nhà cung cấp');
            $table->string('supplier_name', 50)->comment('Tên');
            $table->string('supplier_image', 100)->nullable()->comment('Hình đại diện');
            $table->char('supplier_phone', 20)->comment('Số điện thoại');
            $table->string('supplier_address', 100)->comment('Địa chỉ');
            $table->string('supplier_email', 100)->unique();
            $table->integer('supplier_status')->default('1')->comment('Trạng thái');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
