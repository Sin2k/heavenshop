<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('coupon_id')->comment('Mã giảm giá');
            $table->string('coupon_name', 50)->comment('Tên phiếu giảm giá');
            $table->string('coupon_start_date', 50)->comment('Ngày bắt đầu');
            $table->string('coupon_end_date', 50)->comment('Ngày kết thúc');
            $table->integer('coupon_time')->comment('Số lượng mã');
            $table->integer('coupon_condition')->comment('Phương thức mã');
            $table->integer('coupon_number')->comment('Phần trăm/Tiền giảm');
            $table->string('coupon_code', 50)->unique();
            $table->string('coupon_used', 100)->nullable()->comment('Người dùng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
