<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communes', function (Blueprint $table) {
            $table->increments('commune_id')->comment('Mã phường/xã');
            $table->string('commune_name', 50)->comment('Tên phường/xã');
            $table->string('commune_type', 30)->comment('Loại');
            $table->unsignedInteger('district_id')->comment('Mã quận/huyện');
            $table->foreign('district_id')->references('district_id')->on('districts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communes');
    }
}
