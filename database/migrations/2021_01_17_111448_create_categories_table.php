<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('category_id')->comment('Mã danh mục');
            $table->string('category_name', 50)->comment('Tên danh mục');
            $table->string('category_slug', 50)->unique()->comment('Slug');
            $table->integer('category_parent')->comment('Danh mục cha');
            $table->integer('category_status')->default('1')->comment('Trạng thái');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
