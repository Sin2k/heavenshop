<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('contact_id')->comment('Mã liên lạc');
            $table->string('contact_address', 100)->comment('Địa chỉ');
            $table->char('contact_phone', 20)->unique()->comment('Số điện thoại');
            $table->string('contact_email', 100)->unique();
            $table->string('contact_url_fanpage', 100)->comment('Đường dẫn Fanpage');
            $table->text('contact_map')->comment('Bản đồ');
            $table->text('contact_fanpage')->comment('Fanpage');
            $table->integer('contact_status')->default('1')->comment('Trạng thái');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
