<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('post_id')->comment('Mã bài viết');
            $table->text('post_title')->comment('Tiêu đề');
            $table->string('post_image', 100)->nullable()->comment('Hình ảnh');
            $table->string('post_slug', 100)->unique()->comment('Slug');
            $table->text('post_description')->comment('Mô tả');
            $table->text('post_content')->comment('Nội dung');
            $table->timestamp('post_date')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('Ngày tạo');
            $table->integer('post_view')->default('0')->comment('Lượt xem');
            $table->integer('post_status')->default('1')->comment('Trạng thái');
            $table->string('post_author', 50)->comment('Tác giả');
            $table->unsignedInteger('category_post_id')->comment('Mã danh mục bài viết');
            $table->foreign('category_post_id')->references('category_post_id')->on('category_posts')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
