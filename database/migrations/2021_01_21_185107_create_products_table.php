<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('product_id')->comment('Mã sản phẩm');
            $table->string('product_name', 50)->comment('Tên');
            $table->string('product_quantity', 50)->comment('Số lượng');
            $table->integer('product_sold')->default('0')->comment('Đã bán');
            $table->String('product_slug', 50)->unique()->comment('Slug');
            $table->text('product_description')->comment('Mô tả');
            $table->text('product_content')->comment('Nội dung');
            $table->text('product_tags')->nullable();
            $table->string('product_cost_price', 50)->comment('Giá nhập');
            $table->string('product_price', 50)->comment('Giá bán');
            $table->string('product_image', 100)->nullable()->comment('Hình đại diện');
            $table->integer('product_view')->default('0')->comment('Lượt xem');
            $table->integer('product_status')->default('1')->comment('Trạng thái');
            $table->unsignedInteger('category_id')->comment('Mã danh mục');
            $table->unsignedInteger('brand_id')->comment('Mã thương hiệu');
            $table->unsignedInteger('supplier_id')->comment('Mã nhà cung cấp');
            $table->foreign('category_id')->references('category_id')->on('categories')->onDelete('cascade');
            $table->foreign('brand_id')->references('brand_id')->on('brands')->onDelete('cascade');
            $table->foreign('supplier_id')->references('supplier_id')->on('suppliers')->onDelete('cascade');
            $table->timestamp('product_date')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('Ngày tạo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
