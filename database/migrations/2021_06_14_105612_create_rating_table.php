<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating', function (Blueprint $table) {
            $table->increments('rating_id')->comment('Mã đánh giá');
            $table->integer('rating')->comment('Số sao đánh giá');
            $table->unsignedInteger('customer_id')->comment('Mã khách hàng');
            $table->unsignedInteger('product_id')->comment('Mã sản phẩm');
            $table->timestamp('rating_date')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('Ngày tạo');
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating');
    }
}
